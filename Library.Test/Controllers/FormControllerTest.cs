﻿using Library.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Test.Controllers
{
    [TestClass]
    public class FormControllerTest
    {
        [TestMethod]
        public void Form()
        {
            FormController controller = new FormController();

            // Act
            ViewResult result = controller.Form() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void FormResult()
        {
            FormController controller = new FormController();


            // Act
            ViewResult result = controller.FormResult("Vasil","Bobrow","Male","4","Some txt", true) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
