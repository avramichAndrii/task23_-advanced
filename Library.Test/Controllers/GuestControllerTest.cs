﻿using Library.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Library.Test.Controllers
{
    [TestClass]
    public class GuestControllerTest
    {
        [TestMethod]
        public void Guest()
        {
            GuestController controller = new GuestController();

            // Act
            ViewResult result = controller.Guest() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
