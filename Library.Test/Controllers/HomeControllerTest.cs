﻿using Library.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Test.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Home()
        {
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Main() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
