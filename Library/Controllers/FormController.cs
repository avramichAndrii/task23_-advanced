﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class FormController : Controller
    {
        public ActionResult Form()
        {
            List<string> listCheck = new List<string>() { "Male", "Female" };
            ViewBag.listCheck = listCheck;
            string[] list = new string[] { "1", "2", "3","4","5" };
            ViewBag.list = list;
            return View();
        }

        [HttpPost]
        public ActionResult FormResult(string name, string surname, string radio, string opt,string txt, bool check)
        {
            ViewBag.name = name;
            ViewBag.surname = surname;
            ViewBag.radio = radio;
            ViewBag.opt = opt.ToString();
            ViewBag.txt = txt;
            ViewBag.check = check.ToString();
            return View();
        }
    }
}